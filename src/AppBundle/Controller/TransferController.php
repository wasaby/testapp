<?php
/**
 * Created by PhpStorm.
 * User: 006094
 * Date: 12.10.2018
 * Time: 9:40
 */

namespace AppBundle\Controller;

require_once __DIR__ . '/../../../vendor/autoload.php';

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Stream;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;



class TransferController extends Controller
{

    /**
     * @Route("/transform")
     */
        public function transformAction(Request $request){

            $ip = $request->getClientIp(); //получим ip клиента
            $req = $request->getQueryString(); // получим запрос
            $logger = new Logger('LOG'); // инициализация логера
            $logger->pushHandler(new StreamHandler(__DIR__.'/logs/log'.date("Y-m-d").'.log', Logger::INFO));



// На всякий случай будем перехватывать ошибки
            try{
            $valcode = strtoupper($_GET['valcode']); // получаем код валюты из GET параметра и переводим его в верхний регистр

// проверим, корректно ли введен код валюты (все символы кода - буквы; длина кода == 3 символа)
            if (ctype_alpha($valcode) && strlen($valcode) == 3) {
// получаем массив данных из json, полученного из API
                $json = file_get_contents('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
                $obj = json_decode($json, true);

//ищем в массиве нужную валюту
                foreach ($obj as $data) {
                    if ($data['cc'] == $valcode) {
// если валюта найдена - возвращаем json, предварительно в заголовках установив кодировку utf-8
                        $response = new JsonResponse();
                        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
                        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
                        $response->setData($data);
// запишем в лог информацию о запросе и ответе
                        $logger->info('INF', array('ip' => $ip,
                            'request' => $req,
                            'response' => $response->getContent()));

                        return $response;
                    }

                }
            }  else {
// если код валюты введен некорректно - выведем сообщение, и сделаем соответствующую запись в лог
                $response ='Некорректно введён код валюты';
                $logger->info('ERR', array('ip' => $ip,
                    'request' => $req,
                    'response' => $response));
                return new Response($response);
            }

            } catch (\Exception $err){
                error_log($err->getMessage());
            }

// если валюта не была найдена - выведем страницу с текстом, что значение валюты не найдено
            $response ='Введённый код валюты не найден';
            $logger->info('ERR', array('ip' => $ip,
                'request' => $req,
                'response' => $response));
            return new Response($response);
        }


// функция возврата лога
    public function logAction(){

            $logfile = __DIR__.'/logs/log'.date("Y-m-d").'.log' ;

            if(file_exists($logfile)){
// прочитаем файл лога, используя Stream, т.к. неизвестно насколько большой может быть лог
        $stream = new Stream($logfile);
// для отображения в реальном времени лога, если файл-лога "большой" - вернем только последние 2500 байт, если "маленький" - весь файл
        $data = file_get_contents($stream,false, null, (filesize ($stream) < 2500) ? null : (filesize ($stream) - 2500));
        return new Response(str_replace('[]','<br>',$data));
            } else {
                return new Response('Лог-файл не найден');
            }

    }
}